Ext.define('Client.view.perspective.admin.header.addons.profile.View', {
  extend: 'Ext.Container',
  padding: 5,
  items: [{
    xtype: 'button',
    ui: 'rounded raised normalcase',
    bind: {
      text: '{user.username}',
      tooltip: '{"mod-user-profiles:PROFILE":translate}'
    },
    reference: 'profileBtn',
    handler: function(button) {
      let dialog = Ext.create('Client.dialog.user.profile.View', {
        user: 'me'
      });
      dialog.showBy(button);
    },
  }]
});
Client.view.perspective.admin.header.addons.profile.View.addStatics({
  order: 100
});
