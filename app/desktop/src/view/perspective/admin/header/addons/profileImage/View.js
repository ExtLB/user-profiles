Ext.define('Client.view.perspective.admin.header.addons.profileImage.View', {
  extend: 'Ext.Container',
  padding: 5,
  items: [{
    xtype: 'image',
    cls: 'circular raised',
    height: 35,
    width: 35,
    alt: 'My Profile Image',
    src: 'resources/shared/images/company-logo.png'
  }]
});
Client.view.perspective.admin.header.addons.profileImage.View.addStatics({
  order: 101
});
