Ext.define('Client.view.user_profiles.View',{
	extend: 'Ext.grid.Grid',
	xtype: 'user_profiles',
	cls: 'user_profilesview',
	requires: [],
	controller: {type: 'user_profiles'},
	viewModel: {type: 'user_profiles'},
	store: {type: 'user_profiles'},
  perspectives: ['main'],
	selectable: { mode: 'single' },
	listeners: {
		select: 'onItemSelected'
	},
	columns: [
		{
			text: 'Name',
			dataIndex: 'name',
			width: 100,
			cell: {userCls: 'bold'}
		},
		{text: 'Email',dataIndex: 'email',width: 230},
		{
			text: 'Phone',
			dataIndex: 'phone',
			width: 150
		}
	]
});
