Ext.define('Client.view.user_profiles.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.user_profiles',

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	}
});
