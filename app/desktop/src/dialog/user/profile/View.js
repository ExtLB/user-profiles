Ext.define('Client.dialog.user.profile.View', {
  extend: 'Ext.Dialog',
  xtype: 'user.profile',
  alias: 'widget.user.profile',
  requires: [
    'Client.dialog.user.profile.ViewModel',
    'Client.dialog.user.profile.ViewController',
    'Ext.tab.Panel',
    'Ext.tab.Bar'
  ],
  controller: {type: 'user.profile'},
  viewModel: {type: 'user.profile'},
  bind: {
    title: '{"mod-user-profiles:MY_PROFILE":translate}'
  },
  closable: true,
  height: 600,
  width: 800,
  listeners: {
    initialize: 'onInitialize'
  },
  layout: 'fit',
  items: [{
    xtype: 'tabpanel',
    reference: 'profileTabs',
    tabBar: {
      docked: 'top',
      layout: {
        type: 'hbox',
        pack: 'start'
      }
    },
    items: [{
      xtype: 'container',
      title: 'Profile',
    }, {
      xtype: 'container',
      title: 'Settings',
    }]
  }]
});
